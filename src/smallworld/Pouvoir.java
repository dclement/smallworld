
package smallworld;

import java.awt.image.BufferedImage;
import javax.swing.JPanel;

/**
 *
 * @author Clement
 */
public class Pouvoir extends JPanel{
    String nom;   //Nom du Pouvoir reference a l'UTBM
    String effet;   //Bonus associé
    int nb_personne;    //Nb de troupes
    BufferedImage lien_image_pouvoir;  //Image du Pouvoir

     
    public Pouvoir(){
        nom="NULL";
        nb_personne = 0;
        effet = "NULL";
        lien_image_pouvoir=null;
     
    }
    
    public Pouvoir(String N, int nb, String E, BufferedImage lien){
        nom=N;
        nb_personne = nb;
        effet = E;
        lien_image_pouvoir=lien;
     
    }
    
    public String GetNom(){
        return nom;
    }
    public void SetNom(String N){
        nom=N;
    }
    public int GetNbPersonnage(){
        return nb_personne;
    }
    public void SetNbPersonnage(int nb){
        nb_personne=nb;
    }
    public String GetEffet(){
        return effet;
    }
    public void SetEffet(String N){
        effet=N;
    }
    public BufferedImage GetImage(){
        return lien_image_pouvoir;
    }
    public void SetImage(BufferedImage N){
        lien_image_pouvoir=N;
    }
   
   
    
}
