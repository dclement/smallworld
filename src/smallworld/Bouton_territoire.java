/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package smallworld;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 *
 * @author Clement
 */
public class Bouton_territoire extends JButton {
    private int PosX;   //Position en X du bouton sur la map
    private int PosY;   //Position en Y du bouton sur la map
    private String nom; //Nom du bouton = a celui du territoire qu'il controle
    private ImageIcon icon;  //Image du bouton 
    private int numero_joueur;   //Numero du joueur qui possede le territoire et donc le bouton
    private boolean block;
        
    public Bouton_territoire(){
        Icon icon = null;       
        PosX=0;
        PosY=0;
        nom="";
        numero_joueur=-1;
        block=false;
        try {
             Image image = ImageIO.read(new File("Images/Vide.jpg"));
             icon = new ImageIcon(image.getScaledInstance(Fenetre.largeur_territoire, Fenetre.hauteur_territoire, java.awt.Image.SCALE_SMOOTH));  
         
        } catch (IOException e) {
                        e.printStackTrace();
        }
        
        this.setIcon(icon);
     
    }
    
    public Bouton_territoire(String n){
    	Icon icon = null;       
        PosX=0;
        PosY=0;
        nom=n;
        numero_joueur=-1;
        block=false;
        try {
            Image image = ImageIO.read(new File("Images/Vide.jpg"));
            icon = new ImageIcon(image.getScaledInstance(Fenetre.largeur_territoire, Fenetre.hauteur_territoire, java.awt.Image.SCALE_SMOOTH));  
         
        } catch (IOException e) {
                        e.printStackTrace();
        }
        
        this.setIcon(icon);
    }

   
    public int getPosX() {
        return PosX;
    }

    public void setPosX(int PosX) {
        this.PosX = PosX;
    }

    public int getPosY() {
        return PosY;
    }

    public void setPosY(int PosY) {
        this.PosY = PosY;
    }
    
    public String getNom(){
    	return nom;
    }
    
    public void setNom(String n){
    	nom=n;
    }
    
    public int getnumeroJoueur(){
    	return numero_joueur;
    }
    
    public void setnumeroJoueur(int n){
    	numero_joueur=n;
    }
    public boolean getBlock() {
        return block;
    }

    public void setBlock(Boolean Block) {
        this.block = Block;
    }
 
    public void Change_image(BufferedImage T){
        Image image = T;
        icon = new ImageIcon(image.getScaledInstance(Fenetre.largeur_territoire, Fenetre.hauteur_territoire, java.awt.Image.SCALE_SMOOTH));
        this.setIcon(icon);
    }
    public void Change_image(Image T){
        
        icon = new ImageIcon(T.getScaledInstance(Fenetre.largeur_territoire, Fenetre.hauteur_territoire, java.awt.Image.SCALE_SMOOTH));
        this.setIcon(icon);
    }
    
}
