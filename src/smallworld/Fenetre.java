/*
  * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package smallworld;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.font.TextAttribute;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.AttributedString;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.*;



/**
 *
 * @author Clement
 */
public class Fenetre extends JFrame {
   
    //Parametre de l'application
    private int largeur_fenetre = 1295;
    private int hauteur_fenetre = 720;
    //Parametre du bouton d'achat
    public static int largeur_bouton = 40;
    public static int hauteur_bouton = 40;
    
    //Parametre des boutons territoires
    public static int largeur_territoire = 50;
    public static int hauteur_territoire = 50;
    
    //Les JPanels de l'application
    private PanelWest Panw = new PanelWest();
    public static Map Carte;
    public static PanelSouth PanAction; 
    private Panneau PanE = new Panneau(largeur_fenetre,hauteur_fenetre);
    
    //Le menu de l'application
    private JMenuBar menuBar = new JMenuBar();
    private JMenu M1 = new JMenu("Fichier");
    private JMenu M1_2 = new JMenu("Nouvelle Partie");
    private JMenu M2 = new JMenu("Edition");
    private JMenu M3 = new JMenu("A propos");
    private JMenuItem item2 = new JMenuItem("Quitter");
    private JRadioButtonMenuItem jrmi1 = new JRadioButtonMenuItem("2 Joueurs");
    private JRadioButtonMenuItem jrmi2 = new JRadioButtonMenuItem("3 Joueurs");
    private JRadioButtonMenuItem jrmi3 = new JRadioButtonMenuItem("4 Joueurs");
    
    //Variables globales
    private boolean control =false;
    private boolean debut =false;
    private static int nb_joueur = 0;
    private int nb_joueur_cree = 0;
    private boolean bouton_redeploie = false;
    private Territoire transfert = null;
    public static ArrayList<Couple> tabcouple;
    public ArrayList<Territoire> tabterritoire;
    public static ArrayList<Joueur> tabjoueur;
    public ArrayList<Peuple> tabpeuple;
    public ArrayList<Pouvoir> tabpouvoir;
    public ArrayList<Bouton_territoire> tabbouton;
    public boolean Ftour = false;
    private int nbcouplechoisie = 0; //Test des couples restants pour le declin
    private JButton redeploiment = new JButton();
    private boolean Fin_partie =false;
    public JTextPane textPane = new JTextPane(); //Texte affiché dans panelSouth rappel des actions du jeu
    public String filInfo =""; //Texte affiché dans textPane
    
    public Fenetre(ArrayList<Couple> L, ArrayList<Territoire> L2, ArrayList<Joueur> L3, ArrayList<Peuple> peup, ArrayList<Pouvoir> pv, ArrayList<Bouton_territoire> bt)
    {
        tabcouple = L;
        tabterritoire = L2;
        tabjoueur = L3;
        tabpeuple = peup;
        tabpouvoir = pv;
        tabbouton = bt;
        this.setSize(largeur_fenetre, hauteur_fenetre);
        this.setTitle("SmallWorld");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.M1_2.addSeparator();

        this.M1_2.add(jrmi1);
        this.M1_2.add(jrmi2);
        this.M1_2.add(jrmi3);

        //Ajout du sous-menu dans notre menu
        this.M1.add(this.M1_2);
        //Ajout d'un séparateur
        this.M1.addSeparator();
        item2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                System.exit(0);
            }
        });
        
        jrmi1.addActionListener(new Controller(2));
        jrmi2.addActionListener(new Controller(3));
        jrmi3.addActionListener(new Controller(4));
          
        this.M1.add(item2);

        //ajout des onglets
        this.menuBar.add(M1);
        this.menuBar.add(M2);
        this.menuBar.add(M3);
        this.setJMenuBar(menuBar);



        this.setLayout(new BorderLayout(1,0));
        Carte = new Map();
        this.add(Panw, BorderLayout.EAST);
        Panw.setVisible(false);
        this.add(Carte, BorderLayout.CENTER);
        this.add(PanE,BorderLayout.WEST);
    
        this.setVisible(true);
    }
    
    public void InitFenetreRedemarrage(ArrayList<Couple> L, ArrayList<Territoire> L2, ArrayList<Joueur> L3, ArrayList<Peuple> peup, ArrayList<Pouvoir> pv, ArrayList<Bouton_territoire> bt,int nb)
    {
        tabcouple = L;
        tabterritoire = L2;
        tabjoueur = L3;
        tabpeuple = peup;
        tabpouvoir = pv;
        tabbouton = bt;
        Panw.setVisible(false);
        this.setVisible(true);
        Carte.removeAll();
          Carte = new Map();
                  this.add(Carte, BorderLayout.CENTER);
        SmallWorld.setNBtour(SmallWorld.getNBtour()*nb);
            SmallWorld.Nb_tour = SmallWorld.Nombre_tour;
            nb_joueur= nb;
            Creer_couple();
            Panw.RAZ();
            try {
                smallworld.SmallWorld.Gamestarted();
            } catch (IOException ex) {
                Logger.getLogger(Fenetre.class.getName()).log(Level.SEVERE, null, ex);
            }

            /*
            tabjoueur.clear();
            for (int i = 0; i < nb; i++){
            	
            	SmallWorld.SetQuel_joueur(i+1);
                tabjoueur.add(new Joueur(Nom_Joueur(),i+1));
                
            }
            
            SmallWorld.SetQuel_joueur(0);// On repasse sur le premier joueur
        */
    }
    
    public static int GetNbjoueur() {
        return nb_joueur;
    }
    
    
    public void Debut_tour(){
       int val = 0;
       Joueur J_courant =(Joueur) SmallWorld.ls_joueur.get(SmallWorld.GetQuel_joueur());
       for (int m = 0 ; m< J_courant.GetTerritoire().size();m++){
              Territoire tmp = (Territoire)J_courant.GetTerritoire().get(m);
              val = val + tmp.getNb_perso();
              tmp.setNb_perso(1);
       }
       J_courant.SetNbPiece((val-J_courant.GetTerritoire().size()) + J_courant.GetNbPiece());
       //SmallWorld.Setfin_tour(false);
       Ftour = false;         
    }
    
    public void SetFenetre(boolean T) {
        Carte.setVisible(!T);
        PanE.setVisible(T);
    }
    
    public static void Refresh_info() {
        PanAction.repaint();
        Carte.repaint();
    }
    
    public static Couple Recup_couple(int i) {
        Couple C= null;
        int j=0,k=0;
        do{
            if(tabcouple.get(k).GetLibre()){
                 j++;
                 C = tabcouple.get(k);  
            }
            k++;
            
        }while(j<i);
        return C;
    }
    
    public static PanelSouth getPanSouth(){
 	   return PanAction;
    }
    
    //Fonction pour ajouter une piece au couple et point-1 au joueur
    public void Gain_piece(int nb){
    	Couple C = Recup_couple(nb);
    	C.SetPieceAjoute(C.GetPieceAjoute()+1);
        int tmp = SmallWorld.GetQuel_joueur(); 
        Joueur Ac = (Joueur)tabjoueur.get(tmp);
        Ac.SetNbPoint(Ac.GetNbPoint()-1);
    }
    
    public class PanelWest extends JPanel {
         private Bouton_achat Button_couple;
         private JButton Valider = new JButton("Valider");
         
        //fonction de redimension du BufferedImage
        public  BufferedImage scale(BufferedImage bi, double scaleValue) {
            AffineTransform tx = new AffineTransform();
            tx.scale(scaleValue, scaleValue);
            AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
            BufferedImage biNew = new BufferedImage((int) (bi.getWidth() * scaleValue), (int) (bi.getHeight() * scaleValue), bi.getType());
            return op.filter(bi, biNew);

        }
         
        public void RAZ(){
            Button_couple.setNum(1);
            Button_couple.setLocation(80, 20);
            Valider.setVisible(false);
            this.repaint();
            this.setVisible(true);
        }
        
        //Redimension du JPanel qui affiche les couples
        public PanelWest() {
            this.setPreferredSize( new Dimension( 200, 200 ) );
            
            this.setLayout(null);
            Button_couple = new Bouton_achat();
            Button_couple.setBounds(80, 20, 30, 30);
            this.add(Button_couple);
            
            Valider.setBounds(50, 520, 100, 40);
            Valider.setVisible(false);
            this.add(Valider);

            this.setVisible(true); 
            
            
            
            //Action du bouton achat --> achat du couple
            Button_couple.addActionListener(new ActionListener()
            {

                @Override
                  public void actionPerformed(ActionEvent e){
                    //suivant le numero du bouton donc de la location
                    
                    switch(Button_couple.getNum()){
                        case 1: if (11-nbcouplechoisie>1){ 
                                    Button_couple.setLocation(80, 105);
                                    Button_couple.setNum(2);}
                                else{
                                    Button_couple.setNum(2);
                                    Button_couple.setVisible(false); //si il reste un seul couple
                                }
                                Gain_piece(Button_couple.getNum()-1);
                        break;
                        case 2: if (11-nbcouplechoisie>2){
                                    Button_couple.setLocation(80, 190);
                                    Button_couple.setNum(3);}
                                else{
                                    Button_couple.setVisible(false); //si il reste deux seuls couples
                                }
                        	Gain_piece(Button_couple.getNum()-1);
                        break;
                        case 3: if (11-nbcouplechoisie>3){
                                    Button_couple.setLocation(80, 275);
                                    Button_couple.setNum(4);}
                                else{
                                    Button_couple.setVisible(false); //si il reste trois seuls couples
                                }
                        	Gain_piece(Button_couple.getNum()-1);
                        break;
                        case 4: if (11-nbcouplechoisie>4){
                                    Button_couple.setLocation(80, 360);
                                    Button_couple.setNum(5);}
                                else{
                                    Button_couple.setVisible(false); //si il reste quatre seuls couples
                                }
                        	Gain_piece(Button_couple.getNum()-1);
                        break;
                        case 5: if (11-nbcouplechoisie>5){
                                    Button_couple.setLocation(80, 445);
                                    Button_couple.setNum(6);}
                                else{
                                    Button_couple.setVisible(false); //si il reste cinqs seuls couples
                                }
                        	Gain_piece(Button_couple.getNum()-1);
                        
                        break;
                        case 6: if(11-nbcouplechoisie>6){
                                    Button_couple.setVisible(false);
                                    Button_couple.setNum(7);
                                }
                                else{
                                    Button_couple.setVisible(false); //si il reste six seuls couples
                                }
                        	Gain_piece(Button_couple.getNum()-1);
                        break;
                    }
                    
                    Valider.setVisible(true);
                }
                
        });
        
        //Action du bouton valider --> choix du couple    
        Valider.addActionListener(new ActionListener()
            {

                @Override
                public void actionPerformed(ActionEvent e) {
                    int tmp = SmallWorld.GetQuel_joueur();  
                    Couple T = Recup_couple(Button_couple.getNum()-1);
                    Choix_couple(T);//on recupere le choix du couple
                    System.out.println("Il a choisit le couple " + T.GetNomCouple());
                    T.SetLibre(false);
                    nbcouplechoisie++;
                    //Si tous les couples de couples ont ete choisi, afficahge du bouton achat
                    if(nbcouplechoisie!=11){
                        Button_couple.setVisible(true);
                    }
                    else
                    {
                        Button_couple.setVisible(false);
                    }
   
                    //determine s'il faut cacher les couples lors du dbut de partie (choix des couples)
                    if(nb_joueur_cree != nb_joueur)
                    {
                        //fonction d'actualisation du panel contenant les couples
                        Panw.RAZ();
                    }
                    else
                    {
                        Panw.RAZ();
                        Panw.setVisible(false);
                        Carte.setVisible(true);
                        //Carte.setSize(largeur_fenetre, hauteur_fenetre-80);
                        PanAction.setVisible(true);
                        PanE.setVisible(false);
                        SmallWorld.SetQuel_joueur(0);
                    }
                    if (control==true){
                        Panw.RAZ();
                        Panw.setVisible(false);
                    }     
                    //action du declin changement des images sur le paneS et bouton de la map
                    if(PanAction!=null){
                        SmallWorld.SetQuel_joueur(tmp);
                        Fenetre.Refresh_info();
                        Joueur J = (Joueur)tabjoueur.get(tmp);
                        ArrayList Arr = (ArrayList)J.GetTerritoire();
                        //Changement des images des boutons que possede le joueur
                        for (int p = 0; p<Arr.size();p++){
                               Territoire te = (Territoire)Arr.get(p);
                                for (Bouton_territoire A : tabbouton){
                                        if(te.getNom()==A.getNom()){
                                            Couple Ct = (Couple) tabcouple.get(J.GetCouple());
                                            A.Change_image(Ct.GetPeuple().GetImage());
                                            A.setnumeroJoueur(J.GetPosition()-1);
                                        }
                                }
                        }
             
                        
                    }
                }
                
                
            });   
             
            this.setVisible(false); 
            
        }

        //Choix du couple par le joueur actuel  
        public void Choix_couple(Couple C){
        	
    		int i=0;
    		int k=0;
    		int point=0;
    		Joueur J;
    		JOptionPane jop1 = new JOptionPane();
    		
    		int t = SmallWorld.GetQuel_joueur(); // on recupere le numero du joueur actuel
    		
    		J = tabjoueur.get(t);// on recupere le joueur pour lui donner ensuite son couple, qu'il a choisi
    		
             System.out.println( "" + J.GetNom() + " doit choisir un couple.");
             System.out.println("--------------------------------");
             
             //On trouve la position du couple dans la liste des couples
             do{
            	 	if(C !=tabcouple.get(i)){
            	 		i ++;
            	 	}
            	 	else{
            	 		k=1;
            	 	}
             }while(i!=11 && k!=1);
             
             J.SetCouple(i);// le joueur recupere le couple
             J.SetNbPiece(C.GetNbPion());
             point = C.GetPieceAjoute() + J.GetNbPoint();// On recupere les points qui sont sur le couple  
             J.SetNbPoint(point);//On met a jour le nombre de point du joueur
             if(!control){
                    if(SmallWorld.getNBtour() == 10 && t<nb_joueur-1){
                        jop1.showMessageDialog(null, "C'est au tour du Joueur "+(t+2)+" de choisir un couple" , "Changement de joueur", JOptionPane.INFORMATION_MESSAGE);
                    }
             
                    nb_joueur_cree++;
                    //tous les joueur ont pris leurs couples de départs, on peut lancer la partie
                    if(nb_joueur_cree==nb_joueur){
                         debut = true;
                         PanAction=new PanelSouth();
                         PanAction.setVisible(true);
                         SmallWorld.F.add(Fenetre.getPanSouth(),BorderLayout.SOUTH);
                         Panw.setVisible(false);
                         Carte.setVisible(true);
                    }
                    else
                    {
                        //Joueur suivant à selectionner son couple
                        SmallWorld.SetQuel_joueur(t+1);
                    }
             }
    }
        
        @Override
        public void paintComponent(Graphics g) {
            if (!tabcouple.isEmpty()){
            
                super.paintComponent(g);
                BufferedImage img;
                BufferedImage imgpv;
                BufferedImage img2;
                BufferedImage img2pv;
                int x = 0;
                int y = 0;
                int i = 0;
                int j = 0;
                Button_couple.setSize(hauteur_bouton, largeur_bouton);
          
            
            
                Font font = new Font("Arial", Font.PLAIN, 19);
                //Affichage normal des 6 couples pour le choix des joueurs/declin
                if(nbcouplechoisie<6){
                    while(i<6){
                
                        Couple C = tabcouple.get(j);
                        if (C.GetLibre()){
                            img = C.GetPeuple().GetImage();
                            img2 = scale(img, 0.4);
                            imgpv = C.GetPouvoir().GetImage();
                            img2pv = scale(imgpv, 0.4);
                            //Image du peuple puis pouvoir
                            g.drawImage(img2, x, y+5, this);
                            g.drawImage(img2pv, x+120, y+5, this);
                            g.setFont(font);
                            //Valeur du couple en nombre de troupes
                            String t = String.valueOf(C.GetNbPion());
                            if (C.GetNbPion()>9){
                                g.drawString(t, x+85, y+80);
                            }else
                            {
                                g.drawString(t, x+92, y+80);
                            }

                            g.drawLine(x, y+87, x+Panw.getSize().width, y+87);
                
                            y += 85;
                            i++;
                        }
                        j++;
            
                    }
                }
                else  //Affichage des couples restants donc <6 
                {
                    while(i<11-nbcouplechoisie){
                
                        Couple C = tabcouple.get(j);
                        if (C.GetLibre()){
                            img = C.GetPeuple().GetImage();
                            img2 = scale(img, 0.4);
                            imgpv = C.GetPouvoir().GetImage();
                            img2pv = scale(imgpv, 0.4);
                            g.drawImage(img2, x, y+5, this);
                            g.drawImage(img2pv, x+120, y+5, this);
                            g.setFont(font);
                            String t = String.valueOf(C.GetNbPion());
                            if (C.GetNbPion()>9){
                                g.drawString(t, x+85, y+80);
                            }else
                            {
                                g.drawString(t, x+92, y+80);
                            }

                            g.drawLine(x, y+87, x+Panw.getSize().width, y+87);
                
                            y += 85;
                            i++;
                        }
                        j++;
                   }
                }
            }
        }
    }

    //Selection du nombre de joueur via les boutons 
    public class Controller implements ActionListener{
        private int nb;
        public Controller(int i){
                nb = i;
        }
        
        @Override
        public void actionPerformed(ActionEvent e) {
            if(Fin_partie){
                try {
                    try {
                        SmallWorld.Redemarrage(nb);
                    } catch (URISyntaxException ex) {
                        Logger.getLogger(Fenetre.class.getName()).log(Level.SEVERE, null, ex);
                    }
                 
                } catch (IOException ex) {
                    Logger.getLogger(Fenetre.class.getName()).log(Level.SEVERE, null, ex);
                }
                Fin_partie = false;
                
            }
            JRadioButtonMenuItem tmp = (JRadioButtonMenuItem)e.getSource();
            tmp.setSelected(false);
            SmallWorld.setNBtour(SmallWorld.getNBtour()*nb);
            SmallWorld.Nb_tour = SmallWorld.Nombre_tour;
            nb_joueur= nb;
            Creer_couple();
            Panw.RAZ();
            try {
                smallworld.SmallWorld.Gamestarted();
            } catch (IOException ex) {
                Logger.getLogger(Fenetre.class.getName()).log(Level.SEVERE, null, ex);
            }

            
            tabjoueur.clear();
            for (int i = 0; i < nb; i++){
            	
            	SmallWorld.SetQuel_joueur(i+1);
                tabjoueur.add(new Joueur(Nom_Joueur(),i+1));
                
            }
            
            SmallWorld.SetQuel_joueur(0);// On repasse sur le premier joueur
            
            for(Joueur j : tabjoueur){
                System.out.println("Nom Joueur : "+j.GetNom());
                System.out.println("--------------------------------");
            }
            
  
        }
 }
 
        //Creer les couples Peuple/Pouvoir grace à un random 
        void Creer_couple(){
            //création de deux tableaux pour éviter de prendre deux foix un peuple ou pouvoir 
            int Tab_p [] = new int[11];
            int Tab_pv [] = new int[11];
            int nb=0;
            int nombre_peuple;
            int nombre_pouvoir;
            for (int i=0;i<11;i++){
                Tab_p[i]=0;
                Tab_pv[i]=0;
            }
            
            tabcouple.clear();
            //Tant que qu'ils restent des peuples/pouvoirs a associer pour un couple
            while(nb!=11){
                nombre_peuple = (int)Math.floor(Math.random()*11);
                while(Tab_p[nombre_peuple]==1)
                {
                     nombre_peuple = (int)Math.floor(Math.random()*11);
                }
                Tab_p[nombre_peuple]=1;
                nombre_pouvoir = (int)Math.floor(Math.random()*11);
                while(Tab_pv[nombre_pouvoir]==1)
                {
                    nombre_pouvoir = (int)Math.floor(Math.random()*11);
                }
                Tab_pv[nombre_pouvoir]=1;
                Peuple T  = (Peuple)tabpeuple.get(nombre_peuple);
                Pouvoir P = (Pouvoir)tabpouvoir.get(nombre_pouvoir);
             
                //Création du couple et ajout dans l'ArrayList associé
                Couple C1 = new Couple(T.GetNom() + "/" + P.GetNom(),T.GetNbPersonnage()+P.GetNbPersonnage(),T,P);
                tabcouple.add(C1);
                nb++;
            }
        
        }
        
        
        public String Nom_Joueur() {
        	
      	  String nom = "";
      	  JOptionPane jop = new JOptionPane(), jop2 = new JOptionPane();   
            
      	  //Pour obliger a saisir un nom
      	  while(nom.isEmpty()==true){
                  //Annule l'action du bouton "annuler"
      		  do{
                        nom = JOptionPane.showInputDialog(null, "Veuillez decliner votre identite !", "Nom du joueur "+ SmallWorld.GetQuel_joueur(), JOptionPane.QUESTION_MESSAGE);
                  }
                  while(nom==null);
      		  //Pour eviter d'avoir deux jouer avec le meme nom
      		  for(Joueur J: tabjoueur){
      			  if(nom.equals(J.GetNom())){
      				  nom = "";
      				  jop2.showMessageDialog(null, "Ce nom est deja pris.\n Veuillez en choisir un autre !" , "Attention !!!", JOptionPane.INFORMATION_MESSAGE);
      			  }
      		  }
      		  
      	  }
      	        
      	  return nom;
          }
    
    
    
    //Ecran d'accueil du jeu
    public class Panneau extends JPanel {
 
        public Panneau(int larg, int haut) {
            this.setPreferredSize( new Dimension( larg, haut) );
            this.setVisible(true);
        }
        public void paintComponent(Graphics g){
                try {
                    
                        //String path = new java.io.File(".").getCanonicalPath();
                        Image img = ImageIO.read(new File("Images/Acceuil.jpg"));
                        
                        //Image de fond
                        g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
                } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                }
                
        }               
    }
    
    //Affichage Carte
   class Map extends JPanel {
       private Bouton_territoire bouton;
       
       //fonction de redimension du BufferedImage
       public  BufferedImage scale(BufferedImage bi, double scaleValue) {
           AffineTransform tx = new AffineTransform();
           tx.scale(scaleValue, scaleValue);
           AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
           BufferedImage biNew = new BufferedImage((int) (bi.getWidth() * scaleValue), (int) (bi.getHeight() * scaleValue), bi.getType());
           return op.filter(bi, biNew);

       }
       
       public Map() {
    	   
           
           this.setLayout(null);
           this.setVisible(true);
           this.setBackground(Color.blue);
           

           for (int i=0;i<SmallWorld.ls_bouton.size();i++){
        	   bouton=(Bouton_territoire) SmallWorld.ls_bouton.get(i);
        	   bouton.addActionListener(new Guerre());
        	   switch(bouton.getNom()){
        	   case "LO43" : bouton.setBounds(20, 20, largeur_territoire, hauteur_territoire);
        	   break;
        	   case "CP51" : bouton.setBounds(200, 20, largeur_territoire, hauteur_territoire);
        	   break;
        	   case "TI09" : bouton.setBounds(700, 35, largeur_territoire, hauteur_territoire);
        	   break;
        	   case "ST40" : bouton.setBounds(830, 20, largeur_territoire, hauteur_territoire);
        	   break;
        	   case "MQ80" : bouton.setBounds(1220, 10, largeur_territoire, hauteur_territoire);
        	   break;
        	   case "ST20" : bouton.setBounds(380, 100, largeur_territoire, hauteur_territoire);
        	   break;
        	   case "PS25" : bouton.setBounds(700, 150, largeur_territoire, hauteur_territoire);
        	   break;
        	   case "IP50" : bouton.setBounds(950, 100, largeur_territoire, hauteur_territoire);
        	   break;
        	   case "TX55" : bouton.setBounds(1050, 90, largeur_territoire, hauteur_territoire);
        	   break;
        	   case "PM11" : bouton.setBounds(1220, 140, largeur_territoire, hauteur_territoire);
        	   break;
        	   case "SO07" : bouton.setBounds(155, 350, largeur_territoire, hauteur_territoire);
        	   break;
        	   case "ST10" : bouton.setBounds(400, 450, largeur_territoire, hauteur_territoire);
        	   break;
        	   case "MR51" : bouton.setBounds(800, 350, largeur_territoire, hauteur_territoire);
        	   break;
        	   case "LC02" : bouton.setBounds(890, 320, largeur_territoire, hauteur_territoire);
        	   break;
        	   case "ST50"  : bouton.setBounds(1150, 325, largeur_territoire, hauteur_territoire);
        	   break;
        	   case "AC20" : bouton.setBounds(20, 450, largeur_territoire, hauteur_territoire);
        	   break;
        	   case "GE01" : bouton.setBounds(150, 535, largeur_territoire, hauteur_territoire);
        	   break;
        	   case "EL47"  : bouton.setBounds(500, 480, largeur_territoire, hauteur_territoire);
        	   break;
        	   case "AG51" : bouton.setBounds(780, 495, largeur_territoire, hauteur_territoire);
        	   break;
        	   case "AV00" : bouton.setBounds(1220, 480, largeur_territoire, hauteur_territoire);
        	   break;
        	   }
        	   
        		   
        	   this.add(bouton);
           }
           //affiche uniquement les territoires definis comme etant au bord de map
           SmallWorld.afficheBord();
            
       }

       @Override
       public void paintComponent(Graphics g) {
           super.paintComponent(g);
           BufferedImage img=null;
           try {
		img = ImageIO.read(new File("Images/CarteOK.jpg"));
	   } catch (IOException e) {
		// TODO Auto-generated catch block
                e.printStackTrace();
	   }
      
           g.drawImage(img,0,0,this);
           String text ="";
           for (Territoire Ter :SmallWorld.ls_territoire){
                Bouton_territoire tmp = null;
                for (Bouton_territoire A : SmallWorld.ls_bouton){
                     if(A.getNom().equals(Ter.getNom())){
                                tmp = A;
                     }
                }
                Font font = new Font("Arial", Font.BOLD, 15);
                
                g.setFont(font);
                g.setColor(Color.orange);
                text = Ter.getNom();
                if(Ter.getNom().equals("GE01")){
                    g.drawString(text, tmp.getX()+largeur_territoire+5, tmp.getY()+hauteur_territoire/2+hauteur_territoire/6);
                    if(Ter.getNb_perso()!=0){
                        text = String.valueOf(Ter.getNb_perso());
                        g.drawString(text, tmp.getX()+largeur_territoire+50, tmp.getY()+hauteur_territoire/2+hauteur_territoire/6);
                    }
                    else
                    {
                        text = String.valueOf(Ter.getNb_monstre_neutre());
                        g.drawString(text, tmp.getX()+largeur_territoire+50, tmp.getY()+hauteur_territoire/2+hauteur_territoire/6);
                    }

                }
                else
                {
                    g.drawString(text, tmp.getX()+largeur_territoire/2-largeur_territoire/4, tmp.getY()+hauteur_territoire+15);
                    if(Ter.getNb_perso()!=0){
                        text = String.valueOf(Ter.getNb_perso());
                        g.drawString(text, tmp.getX()+largeur_territoire/2, tmp.getY()+hauteur_territoire+30);
                    }
                    else
                    {
                        text = String.valueOf(Ter.getNb_monstre_neutre());
                        g.drawString(text, tmp.getX()+largeur_territoire/2, tmp.getY()+hauteur_territoire+30);
                    }
                
                }
            }    
       }
       
       ArrayList<Bouton_territoire> getTabBouton(){
    	   return tabbouton;
       }
   }
   
   //Action lors d'un click sur un territoire (via le bouton)
   public class Guerre implements ActionListener{
        private boolean erreur = false;
        private boolean Annuler = false;
	   
       public Guerre(){
               
       }
       
       @Override
       public void actionPerformed(ActionEvent e) {
    	  
    	  int num_joueur=(int) SmallWorld.GetQuel_joueur();
          Joueur J=null;
          if(num_joueur!=tabjoueur.size()){
              J=(Joueur) tabjoueur.get(num_joueur);
          }
          else
          {
              J=(Joueur) tabjoueur.get(0);
          }
    	  Territoire T_choix=null;
          if(SmallWorld.de_renfort()){
              J.SetNbPiece(J.GetNbPiece()+1);
              filInfo = textPane.getText();
              filInfo = filInfo + "Dé de renfort : " + J.GetNom() + "Vous gagnez 1 troupe supplémentaire. \n";        
              Refresh_info();
          }
          
    	  int nb_pion=J.GetNbPiece();
    	  Bouton_territoire comp=(Bouton_territoire) e.getSource();
          Annuler = false;
          //Verification que l'on ne veut pas conquerir un territoire qui nous appartient
    	  if (comp.getnumeroJoueur()!=num_joueur){
              //Il lui reste des troupes pour attaquer
            if (nb_pion>0){
                if(SmallWorld.getAttaque() == true){
        	   
        	  for(Territoire ter : tabterritoire){
        		  if(ter.getNom().equals(comp.getNom())){
        			  T_choix=ter;
        		  }
        	  }
                  if(   "Calculette".equals(SmallWorld.ls_couple.get(J.GetCouple()).GetPouvoir().GetNom()) || "Insomniaque".equals(SmallWorld.ls_couple.get(J.GetCouple()).GetPouvoir().GetNom())){
                        int nb = SmallWorld.BonusPouvoir(J,"Conquete");
                        //Calculette meme nb d'attaquant que def donc une troupe en plus 
                        if(nb==0){
                             if(T_choix.getNb_monstre_neutre()<=nb_pion){
                            //Il faut 1 troupe en plus au minimum pour conquerir sans les bonus   
                            if(T_choix.getNb_perso()<=nb_pion){
                                nb_pion=pop_nb_pion();
                            
                                if (Annuler==false){
                                    if(nb_pion>J.GetNbPiece() || nb_pion<T_choix.getNb_perso() || nb_pion<T_choix.getNb_monstre_neutre()){
                                         do{
                                            nb_pion=pop_nb_pion();
                                         }while(nb_pion>J.GetNbPiece() || nb_pion<T_choix.getNb_perso() || nb_pion<T_choix.getNb_monstre_neutre());
                                    }
                                    filInfo = textPane.getText();
                                    filInfo = filInfo + J.GetNom() + " attaque le territoire " + T_choix.getNom() + " avec " + nb_pion + " troupes. \n";
                      
                                    Refresh_info();
                                    SmallWorld.conquete(J,T_choix,nb_pion,false);
                                    
                                }
                            }
                             }
                        }
                        //Insomniaque -1 troupe pour attaquer une zone
                        if(nb==2){
                            if(T_choix.getNb_monstre_neutre()-1<=nb_pion){
                            //Il faut 1 troupe en plus au minimum pour conquerir sans les bonus   
                            if(T_choix.getNb_perso()-1<=nb_pion){
                                nb_pion=pop_nb_pion();
                            
                                if (Annuler==false){
                                    if(nb_pion>J.GetNbPiece() || nb_pion<T_choix.getNb_perso()-1 || nb_pion<T_choix.getNb_monstre_neutre()-1 || nb_pion==0){
                                         do{
                                            nb_pion=pop_nb_pion();
                                         }while(nb_pion>J.GetNbPiece() || nb_pion<T_choix.getNb_perso()-1 || nb_pion<T_choix.getNb_monstre_neutre()-1 || nb_pion==0);
                                    }
                                    filInfo = textPane.getText();
                                    filInfo = filInfo + J.GetNom() + " attaque le territoire " + T_choix.getNom() + " avec " + nb_pion + " troupes. \n";
                      
                                    Refresh_info();
                                    SmallWorld.conquete(J,T_choix,nb_pion,false);
                                    
                                }
                            }
                             }
                        }
                  }
                  else
                  {  
                        if(T_choix.getNb_monstre_neutre()<nb_pion){
                        //Il faut 1 troupe en plus au minimum pour conquerir sans les bonus   
                            if(T_choix.getNb_perso()<nb_pion){
                                nb_pion=pop_nb_pion();
                                if (Annuler==false){
                                    if(nb_pion>J.GetNbPiece() || nb_pion<=T_choix.getNb_perso() || nb_pion<=T_choix.getNb_monstre_neutre()){
                                         do{
                                            nb_pion=pop_nb_pion();
                                         }while(nb_pion>J.GetNbPiece() || nb_pion<=T_choix.getNb_perso() || nb_pion<=T_choix.getNb_monstre_neutre());
                                    }
                                    filInfo = textPane.getText();
                                    filInfo = filInfo + J.GetNom() + " attaque le territoire " + T_choix.getNom() + " avec " + nb_pion + " troupes. \n";
                      
                                    Refresh_info();
                                    if( "Ami de bar".equals(SmallWorld.ls_couple.get(J.GetCouple()).GetPouvoir().GetNom())){
                                        SmallWorld.conquete(J,T_choix,nb_pion,true);
                                    }
                                    else
                                    {    
                                        SmallWorld.conquete(J,T_choix,nb_pion,false);
                                    }
                                }
                            }
                        }
                  }
                }
    
            } 
          }
          else
          {
              //Il se trouve en phase de redeploiment
              if(SmallWorld.getAttaque() == false){
                   for(Territoire ter : tabterritoire){
        	
        		  if(ter.getNom() == comp.getNom()){
        			  T_choix=ter;
        		  }
        	  }
                   
                  if(bouton_redeploie == true){
                        //Il choisi le territoire ou les troupes vont partir
                        
                        if (SmallWorld.Redeploie == false){ 
                            //1 troupe sur le territoire interdiction de redeploier
                            if(T_choix.getNb_perso()!=1){
                            
                                JOptionPane jop = new JOptionPane();
                                jop.showMessageDialog(null, "Le territoire "+T_choix.getNom()+" possède " +T_choix.getNb_perso() + " troupes" , "Information Troupes", JOptionPane.INFORMATION_MESSAGE);
                                do{
                                    nb_pion=pop_nb_pion();
                                }
                                while(nb_pion>=T_choix.getNb_perso());
                                if (nb_pion!=0){
                                    jop.showMessageDialog(null, "Le joueur "+J.GetNom()+" est en action de redéploiment avec "+ nb_pion , "Redéploiment", JOptionPane.INFORMATION_MESSAGE);
                                    SmallWorld.redeploiement(J,T_choix,nb_pion);
                                    transfert = T_choix;
                                    redeploiment.setEnabled(false);
                                }
                            }
                            else
                            {
                                JOptionPane jop = new JOptionPane();
                                jop.showMessageDialog(null, "Le territoire "+T_choix.getNom()+" possède " +T_choix.getNb_perso() + " troupes" , "Impossible de faire un redéploiement", JOptionPane.WARNING_MESSAGE);
                            }
                        }
                        else
                        {
                            //Territoire qui accueil les troupes
                           
                            T_choix.setNb_perso(T_choix.getNb_perso()+SmallWorld.Mouvement_Troupe);
                            transfert.setNb_perso(transfert.getNb_perso()-SmallWorld.Mouvement_Troupe);
                            redeploiment.setEnabled(true);
                            filInfo = textPane.getText();
                            filInfo = filInfo + J.GetNom() + " transfert " + SmallWorld.Mouvement_Troupe + " troupes dans le territoire " +T_choix.getNom() +" . \n";
                            SmallWorld.Mouvement_Troupe=0;
                            SmallWorld.Redeploie = false;
                            Refresh_info();
                        }
                  }
                }
          }
       }
      
       //Fonction qui test si un char a ete entre
       public boolean testErreur (String M){
        char [] s =M.toCharArray();
        boolean testerreurfrappe= false;

        for (int i=0; i<s.length;i++){
            if(!Character.isDigit(s[i]) )
            {
                testerreurfrappe= true;
            }
        }
        return testerreurfrappe;
        } 
       
       public int pop_nb_pion() {
       		String nb;
       		int nb_pion = 0;
	        JOptionPane jop = new JOptionPane(), jop2 = new JOptionPane();
                Annuler = false;
	        do{
                    nb = jop.showInputDialog(null, "Entrez du nombre de pions", "Choix du nombre de pions", JOptionPane.QUESTION_MESSAGE);
                    if (nb!=null){
                        if(!nb.isEmpty()){
                            if (testErreur(nb)){
                                //Au moins un char de rentrer
                                erreur = true;
                            }  
                            else
                            {
                                //Que des integers donnés
                                nb_pion=Integer.parseInt(nb);
                                erreur =false;
                            }
                        }
                        else
                        {
                        erreur = true;  
                        }
                    }
                    else
                    {
                        Annuler = true;
                    }
                    
                }
                while(erreur == true && Annuler == false);
                erreur = false;
                if(Annuler == false){
                    return nb_pion;
                }
                else
                {
                    return 0;
                }
       }
   }
   
   //Information du joueur (visuel en bas de l'appli)
   public class PanelSouth extends JPanel{
	   
	   private Joueur J_courant;
	   private JButton declin = new JButton();
	   private JButton fin_attaque = new JButton();
	   private JButton fin_tour = new JButton();
           private JScrollPane scrollPane = new JScrollPane( textPane );
	  
           
	   public  BufferedImage scale(BufferedImage bi, double scaleValue) {
           AffineTransform tx = new AffineTransform();
           tx.scale(scaleValue, scaleValue);
           AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
           BufferedImage biNew = new BufferedImage((int) (bi.getWidth() * scaleValue), (int) (bi.getHeight() * scaleValue), bi.getType());
           return op.filter(bi, biNew);

       }
	   public PanelSouth(){
		   
		   
		   
		   this.setPreferredSize( new Dimension( largeur_fenetre, 80 ) );
                   this.setLayout(null);
		   
                   redeploiment.addActionListener(new ActionListener() {
	           @Override
	           public void actionPerformed(ActionEvent arg0) {
                       //on ne peut plus attaquer un territoire mais modifier les troupes
                       SmallWorld.Redeploie=false;
                       bouton_redeploie = true;
                       declin.setVisible(false);
                       if(fin_tour.isVisible()){
                           fin_tour.setVisible(false);
                       }
                       else
                       {
                           fin_tour.setVisible(true);
                           redeploiment.setVisible(false);
                           bouton_redeploie=false;
                       }
	           }
                   });
                   
                   
		   declin.addActionListener(new ActionListener() {
	           @Override
	           public void actionPerformed(ActionEvent arg0) {
                       //il reste des couples pour effectuer le declin
                       if(nbcouplechoisie!=11){
                       
                            //affichage ou non des couples pour l'action du declin
                            if(Panw.isVisible()==true){
                                Panw.setVisible(false);
                            }
                            else
                            {
                               Panw.setVisible(true);
                            }
                            //informe action effectue lors du bouton
                            control=true;
                            declin.setVisible(false);
                       }
                       else
                       {
                              JOptionPane.showMessageDialog(PanelSouth.this,
            "Il n'y a plus de couples disponibles pour effectuer le déclin", "Information",
            JOptionPane.INFORMATION_MESSAGE);
                           declin.setVisible(false);
                       }
	           }
	       });
		   
		   fin_attaque.addActionListener(new ActionListener(){
			  @Override 
			  public void actionPerformed(ActionEvent arg0){
				  SmallWorld.setAttaque(false);
                                  declin.setVisible(false);
                                  redeploiment.setVisible(true);
                                  fin_attaque.setVisible(false);
                                  fin_tour.setVisible(true);
				  Carte.repaint();
			  }
		   });
		   
		   fin_tour.addActionListener(new ActionListener(){
				  @Override 
				  public void actionPerformed(ActionEvent arg0){
					  Ftour = true;
                                          Carte.repaint();
                                          if(!declin.isVisible()){
                                              declin.setVisible(true);
                                          }
                                          SmallWorld.Redeploie=false;
                                          SmallWorld.setAttaque(true);// On remet l'attaque en vrai pour
											// le prochain joueur
                                          //Calcul des points gagné en fct des territoires
                                          Joueur jFintour = (Joueur)tabjoueur.get(SmallWorld.GetQuel_joueur());
                                          int point = jFintour.GetNbPoint();// les points qu'il a deja
    	                                  point += jFintour.ls_terjou.size();//on additionne les points 
                                          jFintour.SetNbPoint(point);//on modifie dans le joueur
                                          SmallWorld.BonusPeuple(jFintour);
                                          int n = SmallWorld.BonusPouvoir(jFintour,"Fin");
                                          //Joueur suivant
                                          SmallWorld.SetQuel_joueur((SmallWorld.GetQuel_joueur()+1)%nb_joueur_cree); // On recup�re quel joueur doit
												// jouer
                                          //System.out.println("N° du joueur " + SmallWorld.GetQuel_joueur());
                                          
                                          redeploiment.setVisible(false);
                                          fin_attaque.setVisible(true);
                                          //Il n'y a plus de couple disponible
                                          if(nbcouplechoisie!=11){
                                                declin.setVisible(true);
                                          }
                                          fin_tour.setVisible(false);
                                          SmallWorld.Nombre_tour--;
                                          
                                          if(SmallWorld.Nombre_tour>nb_joueur){
                                              filInfo = textPane.getText();
                                              filInfo = filInfo + SmallWorld.Nombre_tour +" Tours restants. \n";
                                              
                                          }
                                          else
                                          {
                                              filInfo = textPane.getText();
                                              filInfo = filInfo + "Ceci est votre dernier tour de jeu. \n";
                                          }
                                          
                                          Joueur jCourant = (Joueur)tabjoueur.get(SmallWorld.GetQuel_joueur());
                                          jCourant.SetNbTerritoire(0);
                                          jCourant.SetAttaqueSubit(0);
                                          //un joueur ne peut plus faire d'action attaque & plus de territoire conquit
                                          if(SmallWorld.Nombre_tour==0 || deadPlayer(jCourant)){
                                              fin_partie();
                                          }
                                          else
                                          {
                                              //Actualiseation de l'affichage de la carte en fonction du joueur qui joue
                                              SmallWorld.afficheBord();
                                              SmallWorld.afficheTerritoires(jCourant);
                                              
                                              //Reprend en main toutes les troupes de toutes ces territoires sauf 1 pour pouvoir attaquer d'autres campagnes
                                              SmallWorld.recupTroupes(jCourant);
                                              //actualisation du profil
                                              Fenetre.PanAction.repaint();
                                              if("Antiseches".equals(SmallWorld.ls_couple.get(jCourant.GetCouple()).GetPouvoir().GetNom())){
                                                  int response = JOptionPane.showConfirmDialog(null,"Utiliser votre pouvoir : Passer votre tour et gagner 5 points", "Pouvoir antisèches", JOptionPane.YES_NO_OPTION);
                                                  if(response != JOptionPane.YES_OPTION){
                                                      return;
                                                  }
                                                  else
                                                  {
                                                      n=SmallWorld.BonusPouvoir(jFintour,"Debut");
                                                      filInfo = textPane.getText();
                                                      filInfo = filInfo + jFintour.GetNom() + " active le pouvoir antisèche, il gagne 5 points mais passe son tour.\n" ; 
                                                      redeploiment.setVisible(false);
                                                      fin_attaque.setVisible(false);
                                                      declin.setVisible(false);
                                                      fin_tour.setVisible(true);
                                                  }
                                              }
                                          }
                                              
                                          
				  }
			   });
		   
		   declin.setBounds(300,25,120,40);
		   declin.setText("Declin");
		   fin_attaque.setBounds(450,25,120,40);
		   fin_attaque.setText("Fin attaque");
                   redeploiment.setBounds(600,25,120,40);
		   redeploiment.setText("Redéploiment");
                   redeploiment.setVisible(false);
		   fin_tour.setBounds(750,25,120,40);
		   fin_tour.setText("Fin tour");
                   fin_tour.setVisible(false);
		   this.add(declin);
		   this.add(fin_attaque);
                   this.add(redeploiment);
		   this.add(fin_tour);
                   this.add(scrollPane);
                   scrollPane.setBounds(891, 0, 390, 80);
                   textPane.setEditable(false);
                  
                   
	   }
	   @Override
       public void paintComponent(Graphics g) {
		   
           super.paintComponent(g);
           
           BufferedImage peuple;
           BufferedImage pouvoir;
           BufferedImage peuple_scaled;
           BufferedImage pouvoir_scaled;
           
           if (debut){
               J_courant =(Joueur) SmallWorld.ls_joueur.get(0);
               SmallWorld.SetQuel_joueur(0);
               debut = false;
           }
           else
           {
               J_courant =(Joueur) SmallWorld.ls_joueur.get(SmallWorld.GetQuel_joueur());
           }
           
           
           peuple=tabcouple.get(J_courant.GetCouple()).GetPeuple().GetImage();
           pouvoir=tabcouple.get(J_courant.GetCouple()).GetPouvoir().GetImage();
           peuple_scaled=scale(peuple,0.3);
           pouvoir_scaled=scale(pouvoir,0.3);
           g.drawString(J_courant.GetNom(), 20, 20);
           
           g.drawString(String.valueOf(J_courant.GetNbPiece()),20,40);
           g.drawString("Pions",40,40);
           g.drawImage(peuple_scaled,100,10,this);
           g.drawImage(pouvoir_scaled,160,10,this);
           g.drawLine(890, 0, 890, 200);
         
           textPane.setText(filInfo);
        
       }
   }
    
   public void fin_partie(){
       
           PanE.setVisible(true);
           PanAction.setVisible(false);
           Carte.setVisible(false);
           nb_joueur_cree=0;
           JOptionPane fin = new JOptionPane();
           //tri les joueurs suivants le nb de points, position 0 de tabjoueur = + de points
           Collections.sort(tabjoueur);
           //Message de fin
           switch(nb_joueur){
               case 2: fin.showMessageDialog(null, "Félicitation \nJoueur  -   Points\n"+tabjoueur.get(0).GetNom()+"  -   "+tabjoueur.get(0).GetNbPoint()+"\n"+tabjoueur.get(1).GetNom()+"   -  "+tabjoueur.get(1).GetNbPoint(), "Fin de partie", JOptionPane.INFORMATION_MESSAGE);
                   break;
               case 3: fin.showMessageDialog(null, "Félicitation \nJoueur  -   Points\n"+tabjoueur.get(0).GetNom()+"  -   "+tabjoueur.get(0).GetNbPoint()+"\n"+tabjoueur.get(1).GetNom()+"   -  "+tabjoueur.get(1).GetNbPoint()+"\n"+tabjoueur.get(2).GetNom()+"   -  "+tabjoueur.get(2).GetNbPoint(), "Fin de partie", JOptionPane.INFORMATION_MESSAGE);
                   break;
               case 4: fin.showMessageDialog(null, "Félicitation \nJoueur  -   Points\n"+tabjoueur.get(0).GetNom()+"  -   "+tabjoueur.get(0).GetNbPoint()+"\n"+tabjoueur.get(1).GetNom()+"   -  "+tabjoueur.get(1).GetNbPoint()+"\n"+tabjoueur.get(2).GetNom()+"   -  "+tabjoueur.get(2).GetNbPoint()+"\n"+tabjoueur.get(3).GetNom()+"   -  "+tabjoueur.get(3).GetNbPoint(), "Fin de partie", JOptionPane.INFORMATION_MESSAGE);
                   break;
           }
          
	   Fin_partie = true;
           
           
           //PanAction =null;
   }
   
   
   //Recherche si des actions sont encore possibles pour le joueur
   public boolean deadPlayer (Joueur J){
       boolean statuts = false;
       
       //Plus de territoires et troupes
       if(J.GetTerritoire().isEmpty() && J.GetNbPiece()==0){
           statuts = true;
       }
       return statuts;
   }
}
