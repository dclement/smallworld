package smallworld;

import java.util.ArrayList;



/**
*
* @author Clement
*/
public class Joueur implements java.lang.Comparable {

	private String nom_joueur;  //Nom du joueur tous caractères autorisés
	private int position;   //Numero qui determine quel joueur doit jouer
	private int nb_territoire; //Nb territoires conquis en 1 tour   
	private int nb_piece;   //Nb de troupes
	private int nb_point;   //Nb de points determinant la victoire final du jeu
        private int index_couple;
        private int attaque_subit; //Nb d'attaques subit en 1 tour
	ArrayList<Territoire> ls_terjou=new ArrayList<>(0); //Liste des territoires du joueur
	
	public Joueur(){
		nom_joueur = "NULL";
		position = 0;
		nb_territoire = 0;
		nb_piece = 0;
		index_couple = 12;
		attaque_subit = 0;
                nb_point= 6;
	}
	
	public Joueur(String N, int p){
		nom_joueur = N;
		position = p;
		nb_territoire = 0;
		nb_piece = p;
		index_couple = 12;
		attaque_subit = 0;
                nb_point= 6;
	}
     
    public String GetNom(){
        return nom_joueur;
    }
    
    public void SetNom(String N){
        nom_joueur=N;
    }
    
    public int GetPosition(){
        return position;
    }
    
    public void SetPosition(int nb){
        position=nb;
    }
    
    public int GetNbTerritoire(){
        return nb_territoire;
    }
    
    public void SetNbTerritoire(int nb){
    	nb_territoire=nb;
    }
    
    public int GetNbPiece(){
        return nb_piece;
    }
    
    public void SetNbPiece(int nb){
    	nb_piece=nb;
    }
    
    public int GetNbPoint(){
        return nb_point;
    }
    
    public void SetNbPoint(int nb){
    	nb_point=nb;
    }
    
    public int GetCouple(){
        return index_couple;
    }
    
    public void SetCouple(int c){
    	index_couple = c;
    }
    
    public int GetAttaqueSubit(){
        return attaque_subit;
    }
    
    public void SetAttaqueSubit(int c){
    	attaque_subit = c;
    }
    
    public ArrayList<Territoire> GetTerritoire(){
    	return ls_terjou;
    }
    
    //methode servant a trié l'arrayList des joueurs suivant leurs nb de points 
    @Override
    public int compareTo(Object other) { 
      int nombre1 = ((Joueur) other).GetNbPoint(); 
      int nombre2 = this.GetNbPoint(); 
      if (nombre1 > nombre2)  return 1; 
      else if(nombre1 == nombre2) return 0; 
      else return -1; 
   } 
    
    
}
