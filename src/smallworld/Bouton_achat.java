/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package smallworld;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 *
 * @author Clement
 */
public class Bouton_achat extends JButton {
    private int PosX=80;   //Position en X du bouton d'achat du couple 
    private int PosY=10;   //Position en Y du bouton d'achat du couple
    private int num;       //Numero servant a identifier quel couple est selectionné et la position du bouton d'achat
    
    
    
    public Bouton_achat(){
        Icon icon = null;
   
        PosX=80;
        PosY=20;
        num=1;
            
        try {          
             Image image = ImageIO.read(new File("Images/panier.jpg"));
             icon = new ImageIcon(image.getScaledInstance(Fenetre.largeur_bouton, Fenetre.hauteur_bouton, java.awt.Image.SCALE_SMOOTH));  
         
        } catch (IOException e) {
                        e.printStackTrace();
        }
        
        this.setIcon(icon);
     
    }

     
    
    public int getPosX() {
        return PosX;
    }

    public void setPosX(int PosX) {
        this.PosX = PosX;
    }

    public int getPosY() {
        return PosY;
    }

    public void setPosY(int PosY) {
        this.PosY = PosY;
    }


    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

  
    
    
}
