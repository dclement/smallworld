
package smallworld;

/**
 *
 * @author Clement
 */
public class Couple {
    private  String nom;    //Nom du couple Peuple/Pouvoir reference UTBM
    private  int nb_pion;   //Nb de troupes du couple Peuple + Pouvoir
    private  int piece_ajoute =0;  //Piece ajouté par les joueurs pour l'achat du couple
    private  Peuple P;      //Son Peuple
    private  Pouvoir Pv;    //Son Pouvoir
    private  boolean libre; //Couple choisi par un joueur ou non
    
    public Couple(){
        nom = "NULL";
        nb_pion = 0;
        P = null;
        Pv = null;
        libre = true;
    };
    
    public Couple(String N, int nb, Peuple Peu, Pouvoir Pou){
        nom = N;
        nb_pion = nb;
        P = Peu;
        Pv = Pou;
        libre = true;
    };
    
    
    public Peuple GetPeuple(){
        return P;
    }
    
    public void SetPeuple(Peuple PE){
        P=PE;
    }
    
    public Pouvoir GetPouvoir(){
        return Pv;
    }
     
    public void SetPouvoir(Pouvoir PO){
        Pv=PO;
    }
    
    
    public String GetNomCouple(){
        return nom;
    };
    public void SetNomCouple(String N){
        nom = N;
    };
    
    public int GetPieceAjoute(){
        return piece_ajoute;
    };
    public void SetPieceAjoute(int N){
        piece_ajoute = N;
    };
    
    
    public int GetNbPion(){
        return nb_pion;
    };
    public void SetNbPion(int N){
        nb_pion = N;
    };
    
    public boolean GetLibre(){
        return libre;
    };
    
    public void SetLibre(boolean l){
        libre = l;
    };
    
    
};

