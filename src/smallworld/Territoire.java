/*
  * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package smallworld;

/**
 *
 * @author Clement
 */
public class Territoire {
    private String categorie;  //Type du territoire reference a l'UTBM
    private String nom;    //Nom du territoire reference a l'UTBM
    private String Bonus;  //Nom du bonus du territoire
    private int nb_perso;  //Nombre de troupes du joueur possedant ce territoire
    private int nb_monstre_neutre;  //Nombre de monstre sauvages au debut de la partie
    private String ls_voisin[];  //Listes des territoires voisins pour la conquete suivante
    private boolean territoire_bord; //Joueurs ne veulent qu'attaquer les territoires du "bord" 

    
    public Territoire(){
        categorie="NULL";
        nom="NULL";
        Bonus="NULL";
        nb_perso=0;
        nb_monstre_neutre=0;
        ls_voisin = null;
        territoire_bord=false;
    }
  
    public Territoire(String cat,String n, String b, int perso, int monstre,String[] voisin, boolean bord){
    	categorie=cat;
    	nom=n;
    	Bonus=b;
    	nb_perso=perso;
    	nb_monstre_neutre=monstre;
    	ls_voisin=voisin;
    	territoire_bord=bord;

    }
    
    
    public String getBonus() {
        return Bonus;
    }

    public void setBonus(String Bonus) {
        this.Bonus = Bonus;
    }

    public String[] getls_voisin() {
        return ls_voisin;
    }

    public void setls_voisin(String[] T) {
        this.ls_voisin = T;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public int getNb_monstre_neutre() {
        return nb_monstre_neutre;
    }

    public void setNb_monstre_neutre(int nb_monstre_neutre) {
        this.nb_monstre_neutre = nb_monstre_neutre;
    }

    public int getNb_perso() {
        return nb_perso;
    }

    public void setNb_perso(int nb_perso) {
        this.nb_perso = nb_perso;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public boolean isTerritoire_bord() {
        return territoire_bord;
    }

    public void setTerritoire_bord(boolean territoire_bord) {
        this.territoire_bord = territoire_bord;
    }
    
 
    
}
