/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package smallworld;

import java.io.FileInputStream;
import java.io.IOException;
import sun.audio.AudioData;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;
import sun.audio.ContinuousAudioDataStream;

/**
 *
 * @author Clement
 */
public class SoundGame {
    private AudioPlayer MGP;
    private ContinuousAudioDataStream loop;
    public SoundGame() throws IOException
    {
        music();
     
    }



    public void music() throws IOException{
        String path = new java.io.File(".").getCanonicalPath();
        MGP = AudioPlayer.player;
        AudioStream BGM;
        AudioData MD;
        loop = null;
        try{
            BGM = new AudioStream(new FileInputStream(path + "/son.wav"));
            MD = BGM.getData();
            loop = new ContinuousAudioDataStream(MD);
        }catch(IOException error){
            System.out.print("file not found");
        }
        MGP.start(loop);
       
    }
    
   public void StopM(){
        MGP.stop(loop);
   }

}