
package smallworld;

import java.awt.image.BufferedImage;

/**
 *
 * @author Clement
 */
public class Peuple {
    String nom;   //Nom du Peuple reference UTBM
    int nb_personnage;  //Nb de troupes
    String capacite;    //Bonus associé
    BufferedImage lien_image_peuple;  //Image du peuple
    
    
    public Peuple(){
        nom="NULL";
        nb_personnage = 0;
        capacite = "NULL";
        lien_image_peuple=null;
     
    }
    
    public Peuple(String N, int nb, String Cap, BufferedImage Lien){
        nom=N;
        nb_personnage = nb;
        capacite = Cap;
        lien_image_peuple=Lien;
    }
    
    public String GetNom(){
        return nom;
    }
    public void SetNom(String N){
        nom=N;
    }
    public int GetNbPersonnage(){
        return nb_personnage;
    }
    public void SetNbPersonnage(int nb){
        nb_personnage=nb;
    }
    public String GetCapacite(){
        return capacite;
    }
    public void SetCapacite(String N){
        capacite=N;
    }
    public BufferedImage GetImage(){
        return lien_image_peuple;
    }
    public void SetImage(BufferedImage N){
        lien_image_peuple=N;
    }
    
}
