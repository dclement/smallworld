
package smallworld;

import java.awt.Image;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import javax.imageio.ImageIO;



/**
 *
 * @author Clement
 */
public class SmallWorld {
	
    //Listes des couples/pouvoirs/peuples/territoires/boutons/joueurs de l'application	
    public static ArrayList<Couple> ls_couple=new ArrayList<>(11);
    private static ArrayList<Peuple> ls_peuple=new ArrayList<>(11);
    private static ArrayList<Pouvoir> ls_pouvoir=new ArrayList<>(11);
    public static ArrayList<Territoire> ls_territoire=new ArrayList<>(20);
    public static ArrayList<Bouton_territoire> ls_bouton=new ArrayList<>(20);
    public static ArrayList<Joueur> ls_joueur=new ArrayList<>();
    
    public static int Nombre_tour = 10;  //Nombre de tours pour chaque joueur
    public static int Nb_tour =0; //Copie du nombre de tours 
    public static Fenetre F;  //Fenetre de l'application
    public static boolean Redeploie = false;  //En cours de redeploiement
    public static int Mouvement_Troupe = 0; //Troupes qui sont redeploiees
    private static int quel_joueur=0;  //Numero du joueur qui joue
    private static boolean attaque=true; //En cours d'attaque
    private static boolean fin_tour=true; //Fini son tour
    
    public static void main(String[] args) throws IOException, URISyntaxException, InterruptedException  {
        //Chargement de toutes les images du jeu / remplissage de toutes les Listes    
        Initialisation(); 
                 
        Monster();
        //Lancement de la fenetre principale de l'application
        F = new Fenetre(ls_couple,ls_territoire,ls_joueur,ls_peuple,ls_pouvoir,ls_bouton);
        F.SetFenetre(true);
              
    }
    
    
    //lancement d'une nouvelle partie  
    public static void Redemarrage(int nb) throws IOException, URISyntaxException{
       ls_couple.clear();
       ls_pouvoir.clear();
       ls_peuple.clear();
       ls_territoire.clear();
       ls_joueur.clear();
       ls_bouton.clear();
       Initialisation(); 
       Monster();
       F.InitFenetreRedemarrage(ls_couple,ls_territoire,ls_joueur,ls_peuple,ls_pouvoir,ls_bouton,nb);
      
       F.SetFenetre(true);
       attaque = true;

    }

    //Monstre neutre bouton territoire
    public static void Monster() throws IOException{
        Image image = ImageIO.read(new File("Images/Monstre_neutre.jpg"));
        for (Territoire T :ls_territoire){
            if(T.getNb_monstre_neutre()!=0){
                for (Bouton_territoire A : ls_bouton){
                    if(T.getNom().equals(A.getNom())){
                        A.Change_image(image);
                    }       
                }
            }
        }
    }
    
    //Lancement du Son
    public static void Gamestarted() throws IOException{
    	SoundGame playWave=new SoundGame();
    }
    
    //Affiche les boutons du "bord" de la carte
    public static void afficheBord(){
          //on cache ceux qui ne sont pas defini en bord de map
          for (Territoire T :ls_territoire){
              if(!T.isTerritoire_bord()){
                   for (Bouton_territoire A : ls_bouton){
                       if(A.getNom().equals(T.getNom())){
                           A.setEnabled(false);
                       }
                   }
              }
          }
    }
    
    //Actualisation de la map apres la conquete d'un territoire / affichage des boutons voisins
    public static void afficheVoisinTerritoire(Territoire T){
          String tab []= T.getls_voisin();
          for(int a=0;a<tab.length;a++){
              String NameT = tab[a];
              for (Territoire Ter :ls_territoire){
                   if(Ter.getNom().equals(NameT)){
                       for (Bouton_territoire A : ls_bouton){
                           if(!A.getBlock() && A.getNom().equals(Ter.getNom())){
                                  A.setEnabled(true);
                           }
                       }
                   }
              }
              
          }
    }
    
    public static void afficheTerritoires(Joueur J){
        if(!J.GetTerritoire().isEmpty()){
             ArrayList Liste = J.GetTerritoire();
             for(int y=0;y<Liste.size();y++){
                    Territoire tmp = (Territoire)Liste.get(y);
                    for (Bouton_territoire A : ls_bouton){
                            if(A.getNom().equals(tmp.getNom())){
                                A.setEnabled(true);
                            }
                    }
                    afficheVoisinTerritoire(tmp);
                 
             }
        } 
    }
    
    //Reprend toutes les troupes sauf 1 unite dans toutes les territoires du joueur
    //Les troupes retirees arrivant dans la main du joueur
    public static void recupTroupes(Joueur J){
        if(!J.GetTerritoire().isEmpty()){
            int nbTroupe = 0;
            for(int nb = 0; nb<J.GetTerritoire().size();nb++){
                   Territoire T = (Territoire)J.GetTerritoire().get(nb);
                   nbTroupe = nbTroupe + (T.getNb_perso()-1);
                   T.setNb_perso(1);
            }
        J.SetNbPiece(nbTroupe + J.GetNbPiece());
        }
    }
    
    //Rédeploiement
    public static void redeploiement(Joueur Jo,Territoire T, int nb){
        Mouvement_Troupe = nb;
        Fenetre.Refresh_info();
        Redeploie = true;     
    }
    
    //Fonction de conquete d'un territoire    
    public static void conquete(Joueur Att, Territoire T, int pion_att, boolean BonusAmi){
    	
    	Joueur Def=null; // joueur defenseur
        System.out.println("Territoire a conquerir " + T.getNom());
        
       	//Recherche du joueur defenseur eventuel
        for(Joueur Jou : Fenetre.tabjoueur){
    		if(Jou.ls_terjou.contains(T) == true){ 
    			Def = Jou;
    		}
	}
        //bonus "Ami de bar"
        if(BonusAmi){
            if(Def==null){
                Att.SetNbPoint(Att.GetNbPoint()+2);
            }
        }
        //Si le territoire est deja conquit par un joueur
        if(Def != null){
                ArrayList L = (ArrayList) Def.GetTerritoire();
                Def.SetNbPiece(Def.GetNbPiece()+T.getNb_perso()-1);
                Def.SetAttaqueSubit(Def.GetAttaqueSubit()+1);
                L.remove(T);
        }
        
        Att.ls_terjou.add(T);// On l'ajoute a l'attaquant
    	//Bonus du pouvoir cafe
        if("Cafe".equals(ls_couple.get(Att.GetCouple()).GetPouvoir().GetNom())){
             T.setNb_perso(pion_att-1);// on ajoute les pions de l'attaquant engages dans la bataille au territoire conquit
             Att.SetNbPiece(Att.GetNbPiece() - pion_att +1);//On retire le nombre de pion pour l'attaque de la main du joueur attaquant
        }
        else
        {
             T.setNb_perso(pion_att);// on ajoute les pions de l'attaquant engages dans la bataille au territoire conquit
             Att.SetNbPiece(Att.GetNbPiece() - pion_att);//On retire le nombre de pion pour l'attaque de la main du joueur attaquant
        }
        T.setNb_monstre_neutre(0);
        Att.SetNbTerritoire(Att.GetNbTerritoire() +1);//On increment le nombre de territoire conquit par le joueur ce tour ci
           
        //changement de l'image sur le bouton pour correspondre a celui qui le possede
        //recherche du bon bouton
        for (Bouton_territoire A : ls_bouton){
            
            if(T.getNom().equals(A.getNom())){
                Couple Ct = (Couple) ls_couple.get(Att.GetCouple());
                A.Change_image(Ct.GetPeuple().GetImage());
                A.setnumeroJoueur(Att.GetPosition()-1);
            }
            
        }
        //Repaint le panel d'info du joueur (surtout pour le nb de pions restants qu'il peut poser)
        afficheVoisinTerritoire(T);
        Fenetre.Refresh_info();
    }
 
    public static boolean de_renfort(){
    	
    	Random rd = new Random();
    	boolean Renfort = false;
    	int nb = rd.nextInt(10);// nombre de renfort gagne
    	
    	if(nb==1)
    	{
    		Renfort=true;
    	}
    	
    	return Renfort;
    }
    
    public static void BonusPeuple(Joueur J){
        switch(ls_couple.get(J.GetCouple()).GetPeuple().GetNom()){
            //1 territoire = 1 troupe en +
            case "Geek" : J.SetNbPiece(J.GetNbPiece()+J.GetTerritoire().size());
                    break;
            //+3 troupes si nombre de territoires divisible par 3    
            case "Fetard" : if(J.GetTerritoire().size()%3==0 && !J.GetTerritoire().isEmpty()){
                                J.SetNbPiece(J.GetNbPiece()+3);
                            }
                    break;
            //+1 troupe par HET conquis    
            case "Doctorant" : for(Territoire T : J.GetTerritoire()){
                                    if(T.getCategorie().equals("HET")){
                                        J.SetNbPiece(J.GetNbPiece()+1);
                                    }
                                }
                    break;
            //+2 troupes si pas de conquis dans le tour    
            case "IUT" : if(J.GetNbTerritoire()==0){
                            J.SetNbPiece(J.GetNbPiece()+2);
                         }
                    break;        
            //+2 troupes si le joueur est attaqué au moins 3 fois    
            case "TC" : if(J.GetAttaqueSubit()==3){
                            J.SetNbPiece(J.GetNbPiece()+2);
                        }
                    break;
            //+1 troupe par CS conquis    
            case "Prepa" : for(Territoire T : J.GetTerritoire()){
                                if(T.getCategorie().equals("CS")){
                                     J.SetNbPiece(J.GetNbPiece()+1);
                                }
                           }
                    break;        
            //+1 troupe par CG conquis    
            case "Fille" : for(Territoire T : J.GetTerritoire()){
                                if(T.getCategorie().equals("CG")){
                                     J.SetNbPiece(J.GetNbPiece()+1);
                                }
                           }
                    break;
            //+1 troupe par territoire avec un engrenage conquis    
            case "Meca" : for(Territoire T : J.GetTerritoire()){
                                if(T.getBonus().equals("engrenage")){
                                     J.SetNbPiece(J.GetNbPiece()+1);
                                }
                           }
                    break;        
            //+1 troupe par TM conquis
            case "EDIM" : for(Territoire T : J.GetTerritoire()){
                                if(T.getCategorie().equals("TM")){
                                     J.SetNbPiece(J.GetNbPiece()+1);
                                }
                           }
                    break;
            //+1 troupe par territoire avec un eolienne conquis    
            case "EE" : for(Territoire T : J.GetTerritoire()){
                                if(T.getBonus().equals("eolienne")){
                                     J.SetNbPiece(J.GetNbPiece()+1);
                                }
                           }
                    break;        
            //+3 troupes si un territoire normal + engrenage + eolienne conquis    
            case "IMSI" :   boolean condition1 =false;
                            boolean condition2 =false;
                            boolean condition3 =false;
                        for(Territoire T : J.GetTerritoire()){
                                if(T.getBonus().equals("engrenage")){
                                     condition1 =true;
                                }
                                if(T.getBonus().equals("eolienne")){
                                     condition2 =true;
                                }
                                if(T.getBonus().equals("null") || T.getBonus().equals("pion")){
                                     condition3 =true;
                                }
                        }
                        
                        if(condition1 && condition2 && condition3 ){
                           J.SetNbPiece(J.GetNbPiece()+3); 
                        }
                    break;        
        }
        
    }
    
    public static int BonusPouvoir(Joueur J,String Text){
        int bonus = -2;
        if("Fin".equals(Text)){
            switch(ls_couple.get(J.GetCouple()).GetPouvoir().GetNom()){
                
                //+1 point si 4 territoires ont ete conquis durant ce tour    
                case "Binome" : if(J.GetNbTerritoire()>=4){
                                    J.SetNbPiece(J.GetNbPiece()+2);
                                }
                    break;
                //+7 points au 1er tour    
                case "Feuille de cours" : if( Nombre_tour<=Nb_tour && Nombre_tour>Nb_tour-ls_joueur.size()){
                                            J.SetNbPoint(J.GetNbPoint()+7);
                                          }
                    break;        
                //+1 point par ST conquis    
                case "Semestre d'etude" : for(Territoire T : J.GetTerritoire()){
                                                if(T.getCategorie().equals("ST")){
                                                    J.SetNbPoint(J.GetNbPoint()+1);
                                                }
                                          }
                    break;        
                //Un territoire conquis aleatoirement est protege pour le tour suivant     
                case "Parrainage" : if(!J.GetTerritoire().isEmpty()){
                                        Random r = new Random();
                                        int random = r.nextInt(J.GetTerritoire().size());
                                        Territoire T = (Territoire)J.GetTerritoire().get(random);
                                        for (Bouton_territoire A : ls_bouton){
                                            A.setEnabled(true);
                                            if(A.getNom().equals(T.getNom())){
                                                A.setEnabled(false);
                                            }
                                        }
                                    }
                                    
                    break;
                //+1 point par CG conquis    
                case "Culture Generale" : for(Territoire T : J.GetTerritoire()){
                                            if(T.getCategorie().equals("CG")){
                                                J.SetNbPoint(J.GetNbPoint()+1);
                                            }
                                          }
                    break;
                //+1 point par CS conquis    
                case "Bosse des maths" : for(Territoire T : J.GetTerritoire()){
                                            if(T.getCategorie().equals("CS")){
                                                J.SetNbPoint(J.GetNbPoint()+1);
                                            }
                                         }
                    break;        
                } 
        }
        else
        {
            //Antiseches
            if("Debut".equals(Text)){
                    J.SetNbPoint(J.GetNbPoint()+5);
            }
            else
            {
                if("Conquete".equals(Text)){
                   
                    switch(ls_couple.get(J.GetCouple()).GetPouvoir().GetNom()){
                        case "Calculette" : bonus = 0;
                            break;
                        case "Insomniaque" : bonus = 2;
                           break;
                    }
                }
            }
        }
        return bonus;
    }
   
    public static int getNBtour(){
    	return Nombre_tour;
    }
    
    public static void setNBtour(int nb){
    	Nombre_tour = nb;
    }
    
    public static void Setfin_tour(boolean a){
   	 	fin_tour = a;
    }
    public static boolean getFin_tour(){
    	return fin_tour;
    }
        
    public static boolean getAttaque(){
    	return attaque;
    }
    
    public static void setAttaque(boolean a){
    	attaque = a;
    }
    
    public static int GetQuel_joueur(){
    	return quel_joueur;
    }
    
    public static void SetQuel_joueur(int nb){
    	quel_joueur = nb;
    }
    
    //Création des Peuples, Pouvoirs territoires en chargant les images associées
    static void Initialisation() throws IOException, URISyntaxException{
                           
        //Ajout des peuples, pouvoirs, territoires crés dans l'ArrayList associé 
        ls_peuple.add(new Peuple("Geek",5,"Conversion",ImageIO.read(new File("Images/Peuples/Geek.jpg"))));
        ls_peuple.add(new Peuple("Fetard",6,"Amusement",ImageIO.read(new File("Images/Peuples/fetard.jpg"))));
        ls_peuple.add(new Peuple("Doctorant",6,"Ami-prof",ImageIO.read(new File("Images/Peuples/Doctorant.jpg"))));
        ls_peuple.add(new Peuple("IUT",4,"Experience",ImageIO.read(new File("Images/Peuples/iut.jpg"))));
        ls_peuple.add(new Peuple("TC",5,"Volonte-Acharnement",ImageIO.read(new File("Images/Peuples/Tronc-commun.jpg"))));
        ls_peuple.add(new Peuple("Prepa",6,"Serieux",ImageIO.read(new File("Images/Peuples/Prepa.jpg"))));
        ls_peuple.add(new Peuple("Fille",6,"Charme",ImageIO.read(new File("Images/Peuples/Fille.jpg"))));
        ls_peuple.add(new Peuple("Meca",5,"Fabriquant",ImageIO.read(new File("Images/Peuples/Meca.jpg"))));
        ls_peuple.add(new Peuple("EDIM",5,"Design",ImageIO.read(new File("Images/Peuples/Transe.jpg"))));
        ls_peuple.add(new Peuple("EE",5,"Coup de foudre",ImageIO.read(new File("Images/Peuples/noeud-noeud.jpg"))));
        ls_peuple.add(new Peuple("IMSI",6,"Cache-cache",ImageIO.read(new File("Images/Peuples/Pousse-carton.jpg"))));
        
        
        ls_pouvoir.add(new Pouvoir("Calculette",2,"Calcul",ImageIO.read(new File("Images/Pouvoirs/Calculette.jpg"))));
        ls_pouvoir.add(new Pouvoir("Antiseches",5,"Triche",ImageIO.read(new File("Images/Pouvoirs/Antiseche.jpg"))));        
        ls_pouvoir.add(new Pouvoir("Binome",5,"Travail d'equipe",ImageIO.read(new File("Images/Pouvoirs/Binomes.jpg"))));
        ls_pouvoir.add(new Pouvoir("Feuille de cours",4,"Aide",ImageIO.read(new File("Images/Pouvoirs/Feuilles_de_cours.jpg"))));
        ls_pouvoir.add(new Pouvoir("Ami de bar",5,"Amitie",ImageIO.read(new File("Images/Pouvoirs/Ami_de_bar.jpg"))));
        ls_pouvoir.add(new Pouvoir("Cafe",4,"Soutient",ImageIO.read(new File("Images/Pouvoirs/Cafe.jpg"))));
        ls_pouvoir.add(new Pouvoir("Insomniaque",4,"Infatigable",ImageIO.read(new File("Images/Pouvoirs/Insomniaque.jpg"))));
        ls_pouvoir.add(new Pouvoir("Semestre d'etude",4,"Connaissance",ImageIO.read(new File("Images/Pouvoirs/Semestre-etude.jpg"))));
        ls_pouvoir.add(new Pouvoir("Parrainage",4,"Parraine",ImageIO.read(new File("Images/Pouvoirs/Parrainage.jpg"))));
        ls_pouvoir.add(new Pouvoir("Culture Generale",3,"CG rapporte",ImageIO.read(new File("Images/Pouvoirs/Culture_Generale.jpg"))));
        ls_pouvoir.add(new Pouvoir("Bosse des maths",3,"CS rapporte",ImageIO.read(new File("Images/Pouvoirs/Bosse_des_maths.jpg"))));
       
        //Récupération des informations dans le fichier d'Init
        String Path = System.getProperty("user.dir");
        Path = Path + "\\Images\\Init.txt";
        BufferedReader reader = new BufferedReader(new FileReader(Path));
        String currentLine;
        
        ArrayList<ArrayList> Global=new ArrayList<>();
        while((currentLine = reader.readLine()) != null)
        {
           String[] tokens = currentLine.split(";");
           ArrayList<String> FileString=new ArrayList<>();
           FileString.addAll(Arrays.asList(tokens));
           Global.add(FileString);   
        }
        
        for(int i =0 ; i < Global.size() ; i++)
        {
            ArrayList<String> tmp = Global.get(i);
            //nombre de voisin
            int difference = tmp.size()-6;
            String[] voisin = new String[difference];
            for(int j = 6 ; j < tmp.size() ; j++)
            {
                voisin[j-6] = tmp.get(j);   
            }
            ls_territoire.add(new Territoire(tmp.get(1),tmp.get(0),tmp.get(2),Integer.parseInt(tmp.get(3)),Integer.parseInt(tmp.get(4)),voisin,Boolean.valueOf(tmp.get(5))));
            
        }
             
        Bouton_territoire MQ80= new Bouton_territoire("MQ80");
 	Bouton_territoire LO43= new Bouton_territoire("LO43");
 	Bouton_territoire PS25= new Bouton_territoire("PS25");
 	Bouton_territoire EL47= new Bouton_territoire("EL47");
 	Bouton_territoire CP51= new Bouton_territoire("CP51");
 	Bouton_territoire AG51= new Bouton_territoire("AG51");
 	Bouton_territoire IP50= new Bouton_territoire("IP50");
 	Bouton_territoire PM11= new Bouton_territoire("PM11");
 	Bouton_territoire LC02= new Bouton_territoire("LC02");
 	Bouton_territoire TI09= new Bouton_territoire("TI09");
 	Bouton_territoire SO07= new Bouton_territoire("SO07");
   	Bouton_territoire GE01= new Bouton_territoire("GE01");
   	Bouton_territoire MR51= new Bouton_territoire("MR51");
   	Bouton_territoire AV00= new Bouton_territoire("AV00");
   	Bouton_territoire AC20= new Bouton_territoire("AC20");
   	Bouton_territoire TX55= new Bouton_territoire("TX55");
   	Bouton_territoire ST10= new Bouton_territoire("ST10");
   	Bouton_territoire ST20= new Bouton_territoire("ST20");
   	Bouton_territoire ST40= new Bouton_territoire("ST40");
   	Bouton_territoire ST50= new Bouton_territoire("ST50");
   	ls_bouton.add(MQ80);
        ls_bouton.add(LO43);
        ls_bouton.add(PS25);
        ls_bouton.add(EL47);
        ls_bouton.add(CP51);
        ls_bouton.add(AG51);
        ls_bouton.add(IP50);
        ls_bouton.add(PM11);
        ls_bouton.add(LC02);
        ls_bouton.add(TI09);
        ls_bouton.add(SO07);
        ls_bouton.add(GE01);
        ls_bouton.add(MR51);
        ls_bouton.add(AV00);
        ls_bouton.add(AC20);
        ls_bouton.add(TX55);
        ls_bouton.add(ST10);
        ls_bouton.add(ST20);
        ls_bouton.add(ST40);
        ls_bouton.add(ST50);
      
    }
    

}
